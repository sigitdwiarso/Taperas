import { configure, shallow, mount } from "enzyme";
import IndexPage from '../pages/component/ParticipantList';
import Adapter from "enzyme-adapter-react-16";
import chai, { expect } from "chai";

configure({ adapter: new Adapter() });

it('Renders without crashing', async () => {
  const wrap = mount(<IndexPage/>)
  expect(wrap.find('MaterialTable')).to.have.lengthOf(1);
  await wrap.instance().fetchData();
  wrap.update();
  expect(wrap.find('.MuiTableRow-root')).to.have.lengthOf(7);
});