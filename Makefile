DIR=deployments/docker
RECIPE=${DIR}/docker-compose.yaml
NAMESPACE=builder-${COMPONENT}
MIGRATION_PATH=`pwd`/migrations/test

include .env
export $(shell sed 's/=.*//' .env)

DIND_PREFIX ?= $(HOME)
ifneq ($(API_URL),)
	REACT_APP_API_URL=$(API_URL)
endif
ifneq ($(HOST_PATH),)
DIND_PREFIX := $(HOST_PATH)
endif
ifeq ($(CACHE_PREFIX),)
	CACHE_PREFIX=/tmp
endif

PREFIX=$(shell echo $(PWD) | sed -e s:$(HOME):$(DIND_PREFIX):)
UID=$(shell whoami)

ifneq ($(CI_PIPELINE_ID),)
	BUILD_ID=$(CI_PIPELINE_ID)
else
  BUILD_ID=local
endif
 
export $(BUILD_ID)

.PHONY : test build

build: 
	docker-compose -f ${RECIPE} -p ${NAMESPACE} build --no-cache nextjs
	docker-compose -f ${RECIPE} -p ${NAMESPACE} build --no-cache nginx

run-image:
	docker-compose -f ${RECIPE} -p ${NAMESPACE} up 