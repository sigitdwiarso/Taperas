import Box from '@material-ui/core/Box'
import Divider from '@material-ui/core/Divider'
import Typography from '@material-ui/core/Typography'

function DataIndividu(props) {
  const { value } = props
  const birthDate = new Date(value.data.info.birth_date)
  return (
        <Box display="flex" p={2} flexDirection="column">
            <Box display="flex" flexDirection="row" flex={1} p={1}>
                <Box width="30%">
                    <Typography>
                        NIK
                    </Typography>
                </Box>
                <Box>
                    <Typography>
                      {value.data.info.NIK}
                    </Typography>
                </Box>
            </Box>
            <Divider />
            <Box display="flex" flexDirection="row" flex={1} p={1}>
                <Box width="30%">
                    <Typography>
                        Nama
                    </Typography>
                </Box>
                <Box>
                    <Typography>
                        {value.data.info.fullname}
                    </Typography>
                </Box>
            </Box>
            <Divider />
            <Box display="flex" flexDirection="row" flex={1} p={1}>
                <Box width="30%">
                    <Typography>
                        Jenis Kelamin
                    </Typography>
                </Box>
                <Box>
                    <Typography>
                        {value.data.info.gender === 1 ? "Laki-Laki" : "Perempuan"}
                    </Typography>
                </Box>
            </Box>
            <Divider />
            <Box display="flex" flexDirection="row" flex={1} p={1}>
                <Box width="30%">
                    <Typography>
                        Tempat Lahir
                    </Typography>
                </Box>
                <Box>
                    <Typography>
                        {value.data.info.birth_place}
                    </Typography>
                </Box>
            </Box>
            <Divider />
            <Box display="flex" flexDirection="row" flex={1} p={1}>
                <Box width="30%">
                    <Typography>
                        Tanggal Lahir
                    </Typography>
                </Box>
                <Box>
                    <Typography>
                        {birthDate.toString()}
                    </Typography>
                </Box>
            </Box>
            <Divider />
            <Box display="flex" flexDirection="row" flex={1} p={1}>
                <Box width="30%">
                    <Typography>
                        Latar Belakang Pendidikan
                    </Typography>
                </Box>
                <Box>
                    <Typography>
                        S2 {/* not define on database */}
                    </Typography>
                </Box>
            </Box>
            <Divider />
            <Box display="flex" flexDirection="row" flex={1} p={1}>
                <Box width="30%">
                    <Typography>
                        Gelar Depan
                    </Typography>
                </Box>
                <Box>
                    <Typography>
                        IR {/* not define on database */}
                    </Typography>
                </Box>
            </Box>
            <Divider />
            <Box display="flex" flexDirection="row" flex={1} p={1}>
                <Box width="30%">
                    <Typography>
                        Gelar Belakang
                    </Typography>
                </Box>
                <Box>
                    <Typography>
                        MP {/* not define on database */}
                    </Typography>
                </Box>
            </Box>
            <Divider />
            <Box display="flex" flexDirection="row" flex={1} p={1}>
                <Box width="30%">
                    <Typography>
                        Agama
                    </Typography>
                </Box>
                <Box>
                    <Typography>
                        Islam {/* not define on database */}
                    </Typography>
                </Box>
            </Box>
            <Divider />
            <Box display="flex" flexDirection="row" flex={1} p={1}>
                <Box width="30%">
                    <Typography>
                        Status Pernikahan
                    </Typography>
                </Box>
                <Box>
                    <Typography>
                        {value.data.info.martial_status === 1 ? "Menikah" : "Sendiri"}
                    </Typography>
                </Box>
            </Box>
            <Divider />
            <Box display="flex" flexDirection="row" flex={1} p={1}>
                <Box width="30%">
                    <Typography>
                        Nama Pasangan
                    </Typography>
                </Box>
                <Box>
                    <Typography>
                        {value.data.info.spouse_name}
                    </Typography>
                </Box>
            </Box>
            <Divider />
            <Box display="flex" flexDirection="row" flex={1} p={1}>
                <Box width="30%">
                    <Typography>
                        Nomor Ponsel
                    </Typography>
                </Box>
                <Box>
                    <Typography>
                        {value.data.info.mobile_number}
                    </Typography>
                </Box>
            </Box>
            <Divider />
            <Box display="flex" flexDirection="row" flex={1} p={1}>
                <Box width="30%">
                    <Typography>
                        Email
                    </Typography>
                </Box>
                <Box>
                    <Typography>
                      {value.data.info.email_address}
                    </Typography>
                </Box>
            </Box>
            <Divider />
            <Box display="flex" flexDirection="row" flex={1} p={1}>
                <Box width="30%">
                    <Typography>
                        Nama Ibu Kandung
                    </Typography>
                </Box>
                <Box>
                    <Typography>
                      {value.data.info.mother_name}
                    </Typography>
                </Box>
            </Box>
            <Divider />
            <Box display="flex" flexDirection="row" flex={1} p={1}>
                <Box width="30%">
                    <Typography>
                        Nomor Peserta Tapera
                    </Typography>
                </Box>
                <Box>
                    <Typography>
                      {value.data.number}
                    </Typography>
                </Box>
            </Box>
            <Divider />
            <Box display="flex" flexDirection="row" flex={1} p={1}>
                <Box width="30%">
                    <Typography>
                        Single Investor Identification (SID)
                    </Typography>
                </Box>
                <Box>
                    <Typography>
                      {value.data.SID}
                    </Typography>
                </Box>
            </Box>
            <Divider />
            <Box display="flex" flexDirection="row" flex={1} p={1}>
                <Box width="30%">
                    <Typography>
                        Investor Fund Unit Account (IFUA)
                    </Typography>
                </Box>
                <Box>
                    <Typography>
                      {value.data.IFUA}
                    </Typography>
                </Box>
            </Box>
            <Divider />

        </Box>
    )
}

export default DataIndividu