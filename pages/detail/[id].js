import fetch from 'node-fetch';
import React, {useState} from 'react';
import { useRouter } from 'next/router'
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import Box from '@material-ui/core/Box';
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Typography from '@material-ui/core/Typography';
import detail from '../../styles/detail.module.css';
import Breadcrumbs from '@material-ui/core/Breadcrumbs';
import Link from '@material-ui/core/Link';
import Title from '../component/Title';

import DataIndividu from './DataIndividu';
import DataAlamat from './DataAlamat';
import DataFinansial from './DataFinansial';
import DataPekerjaan from './DataPekerjaan';
import DataManfaat from './DataManfaat';

export async function getServerSideProps(context) {
  
  let payload = {
    "id": context.params.id
  }

  const res = await fetch("http://localhost:8000/api/tapera_participant/ParticipantService/GetParticipantById",{
    method: 'POST',
    body: JSON.stringify(payload),
    headers: {'Content-Type': 'application/json'}
  })

  const data = await res.json()
  return {
    props:{
      data
    }
  }

}

function handleClick(event) {
  event.preventDefault();
  console.info('You clicked a breadcrumb.');
}

function TabPanel(props) {
  const { children, value, index, ...other } = props;

  return (
    <div
      role="tabpanel"
      hidden={value !== index}
      id={`simple-tabpanel-${index}`}
      aria-labelledby={`simple-tab-${index}`}
      {...other}
    >
      {value === index && (
        <Box p={3}>
          <Typography>{children}</Typography>
        </Box>
      )}
    </div>
  );
}

TabPanel.propTypes = {
  children: PropTypes.node,
  index: PropTypes.any.isRequired,
  value: PropTypes.any.isRequired,
};

function a11yProps(index, props) {
  return {
    id: `simple-tab-${index}`,
    'aria-controls': `simple-tabpanel-${index}`,
  };
}

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 3,
    backgroundColor: theme.palette.background.TabPanel,
  }
}));


function Page (props) {
  const classes = useStyles();
  const [value, setValue] = React.useState(0);
  const { data } = props

  const handleChange = (event, newValue) => {
    setValue(newValue);
  };

  
  return (
    <div className={classes.root}>
      <Breadcrumbs aria-label="breadcrumb">
          <Link color="inherit" href="/" onClick={handleClick}>
              Daftar Peserta
          </Link>
          <Link
              color="textPrimary"
              href="#"
              onClick={handleClick}
              aria-current="page"
          >
              Detail Peserta
          </Link>
      </Breadcrumbs>
      <Box display="flex" p={1}>
          <Box p={1} flexGrow={1}>
              <Title children="Detail Peserta"></Title>
          </Box>

          <Box p={1}>
              <Box display="flex" flexDirection='row'>
                  <Box flexGrow={1}>
                      <Typography>
                          Status Peserta
                      </Typography>
                  </Box>
                  <Box>
                      <Typography>
                          Aktif
                      </Typography>
                  </Box>
              </Box>
              <Box display="flex" flexDirection='row'>
                  <Box flexGrow={1}>
                      <Typography>
                          Status Pendaftaran
                      </Typography>
                  </Box>
                  <Box ml={4}>
                      <Typography>
                          Valid
                      </Typography>
                  </Box>
              </Box>
          </Box>
      </Box>

      <AppBar position="static">
        <Tabs value={value} onChange={handleChange} aria-label="simple tabs example">
          <Tab label="Data Individu" {...a11yProps(0)} />
          <Tab label="Data Alamat" {...a11yProps(1)} />
          <Tab label="Data Finansial" {...a11yProps(2)} />
          <Tab label="Data Pekerjaan" {...a11yProps(3)} />
          <Tab label="Data Manfaat" {...a11yProps(4)} />
        </Tabs>
      </AppBar>
      <TabPanel value={value} index={0}>
        <DataIndividu value={props}/>
      </TabPanel>
      <TabPanel value={value} index={1}>
        <DataAlamat value={props}/>
      </TabPanel>
      <TabPanel value={value} index={2}>
        <DataFinansial value={props}/>
      </TabPanel>
      <TabPanel value={value} index={3}>
        <DataPekerjaan value={props}/>
      </TabPanel>
      <TabPanel value={value} index={4}>
        <DataManfaat value={props}/>
      </TabPanel>
    </div>
  );
}

export default Page