import Data from '../data/data';

const service = {
  get: function(url, payload) {
    if (url === "participant") {
      return Data.participant.get(payload);
    }

    // add here for customer another service 
  }
}

export default service
