import Axios from 'axios';
import Setting from '../setting';

const service = {
  get: function(url, payload) {
    return Axios.get(Setting.apiURL + url, payload);
  },

  getWithParam: function(url, payload, param) {
    return Axios.get(Setting.apiURL+url+"/"+param, payload);
  },

  post: function(url, payload) {
    return Axios.post(Setting.apiURL+url, payload);
  }
}

module.exports = service;