const Settings = {
  apiURL: process.env.REACT_APP_API_URL,
  domock: process.env.REACT_APP_API_URL ? false : true
};

export default Settings;
